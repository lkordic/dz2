# Opis aplikacije #

Aplikacija se sastoji od šest Activitya. Glavni sadrži ImageButtone koji omogućuju odabir jedne od četiri ponuđenih pretvorbi. Odabirom jedne od pretvorbi eksplicitnim intentom pokreće se pripadna aktivnost. Svaka pretvorba ima svoju, zasebnu aktivnost u kojoj se unosi željena vrijednost za pretvorbu. Isto tako pomoću spinnera korisnik odabire jedinice za pretvorbu. Pritiskom na gumb Convert, uneseni podaci prikupljaju se i šalju u Result Activity korištenjem extra podataka.
U tom activityu, korištenjem switch case grananja, odabire se formula za izračun određene pretvorbe. Za svaku odabranu koverziju sa prethodno spomenutih spinnera extra podacima šalje se integer prema kojemu se odabire case.

Napravljeni su i layouti za landscape orijentaciju zaslona koji su smješteni u mapu layouts-land. Na taj način aplikacija automatski postavlja layoute iz te mape kada je uređaj okrenut horizontalno.

## Pomoćni materijali  ##
https://loomen.carnet.hr/pluginfile.php/772054/mod_resource/content/1/LV2%20-%20predložak%282017%29.pdf

Za pomoć pri dohvaćanju odabira iz spinnera korišten je primjer sa navedenog linka.
http://stackoverflow.com/questions/16581536/setonitemselectedlistener-of-spinner-does-not-call