package com.example.lukakordi.dz2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class DistanceActivity extends Activity implements View.OnClickListener {

    public static final String RESULT_KEY = "result";
    public static final String CASE_KEY = "case";
    EditText etdistance;
    Spinner spinnerDistance;
    Button bConvertDistance;
    String spinnerItem;
    double inputValue;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance);
        this.setUpUI();
    }

    private void setUpUI() {
        this.etdistance = (EditText) findViewById(R.id.etDistance);
        this.spinnerDistance = (Spinner) findViewById(R.id.spinnerDistance);
        this.bConvertDistance = (Button) findViewById(R.id.btnConvertDistance);

        this.bConvertDistance.setOnClickListener(this);
        this.spinnerDistance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerItem = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        intent = new Intent(this, ResultActivity.class);

        switch(spinnerItem){
            case "Meters to Inches":
                intent.putExtra(CASE_KEY, 3);
                break;
            case "Meters to Feet":
                intent.putExtra(CASE_KEY, 4);
                break;
            case "Inches to Meters":
                intent.putExtra(CASE_KEY, 5);
                break;
            case "Inches to Feet":
                intent.putExtra(CASE_KEY, 6);
                break;
            case "Feet to Inches":
                intent.putExtra(CASE_KEY, 7);
                break;
            case "Feet to Meters":
                intent.putExtra(CASE_KEY, 8);
                break;
        }
        if(etdistance.getText().toString().isEmpty() || etdistance.length() == 0 || etdistance.getText().toString().equals(""))
        {
            etdistance.setError("Enter Value");
        }
        else
        {
            intent.putExtra(RESULT_KEY, Double.valueOf(etdistance.getText().toString()));
            startActivity(intent);
        }
    }
}
