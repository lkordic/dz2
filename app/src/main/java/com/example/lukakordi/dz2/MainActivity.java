package com.example.lukakordi.dz2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends Activity implements View.OnClickListener {

    ImageButton ibSpeed, ibDistance, ibTemperature, ibAngle;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setUpUI();
    }

    private void setUpUI() {
        this.ibSpeed = (ImageButton) findViewById(R.id.ibSpeed);
        this.ibDistance = (ImageButton) findViewById(R.id.ibDistance);
        this.ibTemperature = (ImageButton) findViewById(R.id.ibTemperature);
        this.ibAngle = (ImageButton) findViewById(R.id.ibAngle);

        this.ibSpeed.setOnClickListener(this);
        this.ibDistance.setOnClickListener(this);
        this.ibTemperature.setOnClickListener(this);
        this.ibAngle.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case(R.id.ibSpeed):
                intent = new Intent(getApplicationContext(), SpeedActivity.class);
                startActivity(intent);
            break;
            case(R.id.ibDistance):
                intent = new Intent(getApplicationContext(), DistanceActivity.class);
                startActivity(intent);
            break;
            case(R.id.ibTemperature):
                intent = new Intent(getApplicationContext(), TemperatureActivity.class);
                startActivity(intent);
            break;
            case(R.id.ibAngle):
                intent = new Intent(getApplicationContext(), AngleActivity.class);
                startActivity(intent);
            break;
        }
    }
}
