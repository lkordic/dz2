package com.example.lukakordi.dz2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SpeedActivity extends Activity implements View.OnClickListener {
    public static final String RESULT_KEY = "result";
    public static final String CASE_KEY = "case";
    TextView tvSpeed;
    Spinner spinnerSpeed;
    EditText etSpeed;
    Button bConvertSpeed;
    String spinnerItem;
    public static final String LOG_TAG = "Luka";

    double inputvalue, result;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speed);
        this.setUpUI();
    }

    private void setUpUI() {
        this.tvSpeed = (TextView) findViewById(R.id.tvSpeed);
        this.spinnerSpeed = (Spinner) findViewById(R.id.spinnerSpeed);
        this.etSpeed = (EditText) findViewById(R.id.etSpeed);
        this.bConvertSpeed = (Button) findViewById(R.id.btnConvertSpeed);

        this.bConvertSpeed.setOnClickListener(this);
        spinnerSpeed.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerItem = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        intent = new Intent(this, ResultActivity.class);

        switch(spinnerItem){
        case "Km/h to M/h":
            intent.putExtra(CASE_KEY, 1);
            break;
        case "M/h to Km/h":
            intent.putExtra(CASE_KEY, 2);
            break;
    }
        if(etSpeed.getText().toString().isEmpty() || etSpeed.length() == 0 || etSpeed.equals(""))
        {
            etSpeed.setError("Enter Value");
        }
        else
        {

            intent.putExtra(RESULT_KEY, Double.valueOf(etSpeed.getText().toString()));
            startActivity(intent);
        }
    }
}
