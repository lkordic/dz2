package com.example.lukakordi.dz2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import static com.example.lukakordi.dz2.R.id.etSpeed;

public class ResultActivity extends Activity {

    TextView tvInputValue, tvInputUnit, tvOutputValue, tvOutputUint;
    Intent intent;
    double inputvalue;
    int slucaj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        this.setUpUI();
    }

    private void setUpUI() {
        this.tvInputValue = (TextView) findViewById(R.id.tvInputValue);
        this.tvInputUnit = (TextView) findViewById(R.id.tvInputUnit);
        this.tvOutputValue = (TextView) findViewById(R.id.tvOutputValue);
        this.tvOutputUint = (TextView) findViewById(R.id.tvOutputUnit);

        intent = this.getIntent();

        inputvalue = intent.getDoubleExtra(SpeedActivity.RESULT_KEY, 0);
        slucaj = intent.getIntExtra(SpeedActivity.CASE_KEY, 0);

        switch (slucaj){
            //Speed Conversion
            case 1:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Kilometers");
                tvOutputValue.setText(String.valueOf(inputvalue * 0.6214));
                tvOutputUint.setText("Miles");

                break;

            //Distance conversion
            case 2:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Miles");
                tvOutputValue.setText(String.valueOf(inputvalue / 0.6214));
                tvOutputUint.setText("Kilometers");
            break;
            case 3:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Meters");
                tvOutputValue.setText(String.valueOf(inputvalue * 39.37));
                tvOutputUint.setText("Inches");
                break;
            case 4:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Meters");
                tvOutputValue.setText(String.valueOf(inputvalue * 3.28));
                tvOutputUint.setText("Feet");
                break;
            case 5:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Inches");
                tvOutputValue.setText(String.valueOf(inputvalue / 39.37));
                tvOutputUint.setText("Meters");
                break;
            case 6:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Inches");
                tvOutputValue.setText(String.valueOf(inputvalue * 0.0833));
                tvOutputUint.setText("Feet");
                break;
            case 7:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Feet");
                tvOutputValue.setText(String.valueOf(inputvalue / 0.0833));
                tvOutputUint.setText("Inches");
                break;
            case 8:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Feet");
                tvOutputValue.setText(String.valueOf(inputvalue / 3.28));
                tvOutputUint.setText("Meters");
                break;

            //Temperature conversion
            case 9:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Celsius");
                tvOutputValue.setText(String.valueOf(((inputvalue * 9/5)+32)));
                tvOutputUint.setText("Fahrenheit");
                break;
            case 10:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Fahrenheit");
                tvOutputValue.setText(String.valueOf((inputvalue - 32) * 5/9));
                tvOutputUint.setText("Celsius");
                break;
            case 11:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Kelvin");
                tvOutputValue.setText(String.valueOf(inputvalue -273.15));
                tvOutputUint.setText("Celsius");
                break;
            case 12:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Celsius");
                tvOutputValue.setText(String.valueOf(inputvalue + 273.15));
                tvOutputUint.setText("Kelvin");
                break;
            case 13:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Kelvin");
                tvOutputValue.setText(String.valueOf((inputvalue * 9/5) - 459.67));
                tvOutputUint.setText("Fahrenheit");
                break;
            case 14:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Fahrenheit");
                tvOutputValue.setText(String.valueOf((inputvalue + 459.67) * 5/9));
                tvOutputUint.setText("Kelvin");
                break;

            //Angle conversion
            case 15:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Deg");
                tvOutputValue.setText(String.valueOf(inputvalue * 1.1111111111111));
                tvOutputUint.setText("Grad");
                break;
            case 16:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Grad");
                tvOutputValue.setText(String.valueOf(inputvalue * 0.9));
                tvOutputUint.setText("Deg");
                break;
            case 17:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Rad");
                tvOutputValue.setText(String.valueOf(inputvalue * 57.295779513082));
                tvOutputUint.setText("Deg");
                break;
            case 18:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Deg");
                tvOutputValue.setText(String.valueOf(inputvalue * 0.017453292519943));
                tvOutputUint.setText("Rad");
                break;
            case 19:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Grad");
                tvOutputValue.setText(String.valueOf(inputvalue *  0.015707963267949));
                tvOutputUint.setText("Rad");
                break;
            case 20:
                tvInputValue.setText(String.valueOf(inputvalue));
                tvInputUnit.setText("Rad");
                tvOutputValue.setText(String.valueOf(inputvalue * 63.661977236758));
                tvOutputUint.setText("Grad");
                break;
        }
    }
}
