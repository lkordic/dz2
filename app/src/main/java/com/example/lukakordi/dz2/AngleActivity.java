package com.example.lukakordi.dz2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class AngleActivity extends Activity implements View.OnClickListener {

    public static final String RESULT_KEY = "result";
    public static final String CASE_KEY = "case";
    EditText etAngle;
    Spinner spinnerAngle;
    Button bConvertAngle;
    String spinnerItem;
    double inputValue;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_angle);
        this.setUpUI();
    }

    private void setUpUI() {
        this.etAngle = (EditText) findViewById(R.id.etAngle);
        this.bConvertAngle = (Button) findViewById(R.id.btnConvertAngle);
        this.spinnerAngle = (Spinner) findViewById(R.id.spinnerAngle);

        this.bConvertAngle.setOnClickListener(this);
        this.spinnerAngle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerItem = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        intent = new Intent(this, ResultActivity.class);

        switch(spinnerItem){
            case "Deg to Grad":
                intent.putExtra(CASE_KEY, 15);
                break;
            case "Grad to Deg":
                intent.putExtra(CASE_KEY, 16);
                break;
            case "Rad to Deg":
                intent.putExtra(CASE_KEY, 17);
                break;
            case "Deg to Rad":
                intent.putExtra(CASE_KEY, 18);
                break;
            case "Grad to Rad":
                intent.putExtra(CASE_KEY, 19);
                break;
            case "Rad to Grad":
                intent.putExtra(CASE_KEY, 20);
                break;
        }
        if(etAngle.getText().toString().isEmpty() || etAngle.length() == 0 || etAngle.getText().toString().equals(""))
        {
            etAngle.setError("Enter Value");
        }
        else
        {
            intent.putExtra(RESULT_KEY, Double.valueOf(etAngle.getText().toString()));
            startActivity(intent);
        }

    }
}
