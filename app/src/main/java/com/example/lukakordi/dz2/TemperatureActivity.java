package com.example.lukakordi.dz2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class TemperatureActivity extends Activity implements View.OnClickListener {

    public static final String RESULT_KEY = "result";
    public static final String CASE_KEY = "case";
    EditText etTemperature;
    Spinner spinnerTemperature;
    Button bConvertTemperature;
    String spinnerItem;
    double inputValue;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature);
        this.setUpUI();

    }

    private void setUpUI() {
        this.etTemperature = (EditText) findViewById(R.id.etTemp);
        this.spinnerTemperature = (Spinner) findViewById(R.id.spinnerTemp);
        this.bConvertTemperature = (Button) findViewById(R.id.btnConvertTemp);

        this.bConvertTemperature.setOnClickListener(this);
        this.spinnerTemperature.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerItem = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        intent = new Intent(this, ResultActivity.class);

        switch(spinnerItem){
            case "Celsius to Fahrenheit":
                intent.putExtra(CASE_KEY, 9);
                break;
            case "Fahrenheit to Celsius":
                intent.putExtra(CASE_KEY, 10);
                break;
            case "Kelvin to Celsius":
                intent.putExtra(CASE_KEY, 11);
                break;
            case "Celsius to Kelvin":
                intent.putExtra(CASE_KEY, 12);
                break;
            case "Kelvin to Fahrenheit":
                intent.putExtra(CASE_KEY, 13);
                break;
            case "Fahrenheit to Kelvin":
                intent.putExtra(CASE_KEY, 14);
                break;
        }
        if(etTemperature.getText().toString().isEmpty() || etTemperature.length() == 0 || etTemperature.getText().toString().equals(""))
        {
            etTemperature.setError("Enter Value");
        }
        else
        {
            intent.putExtra(RESULT_KEY, Double.valueOf(etTemperature.getText().toString()));
            startActivity(intent);
        }
    }
}
